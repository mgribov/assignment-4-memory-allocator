#define _DEFAULT_SOURCE

#include <stdio.h>
#include <string.h>

#include "mem.h"
#define BLOCK_SIZE 32
#define BLOCK_SIZE_2 64
#define PAGE_SIZE 4096
#define HEAP_PAGE_SIZE 8192
#define HEAP_PAGE_SIZE_2 20480
#define BIG_BLOCK_SIZE 15000
#define HEAP_INITIAL_VALUE 1

int main(void) {
  printf("Выделение и освобождение памяти\n");
  heap_init(HEAP_INITIAL_VALUE);

  void* alloc = _malloc(BLOCK_SIZE);
  if(alloc == NULL) fprintf(stderr, "%s", "Error: malloc block failed\n");
  printf("Аллоцирование одного блока\n");
  debug_heap(stdout, HEAP_START);
  _free(alloc);
  printf("Освобождение одного блока\n");
  debug_heap(stdout, HEAP_START);
  if(munmap(HEAP_START, HEAP_PAGE_SIZE)==0){
    printf("Тест пройден успешно, программных ошибок не зафиксировано\n");
  } else {
    fprintf(stderr, "%s", "Error: Ошибка munmap\n");
  }

  printf("Выделение двух блоков и освобождение одного\n");
  heap_init(HEAP_INITIAL_VALUE);

  void* alloc1 = _malloc(BLOCK_SIZE);
  if(alloc1 == NULL) fprintf(stderr, "%s", "Error: malloc block1 failed\n");
  void* alloc2 = _malloc(BLOCK_SIZE_2);
  if(alloc2 == NULL) fprintf(stderr, "%s", "Error: malloc block2 failed\n");
  printf("Аллоцирование двух блоков\n");

  debug_heap(stdout, HEAP_START);
  _free(alloc1);
  printf("Освобождение первого блока\n");
  debug_heap(stdout, HEAP_START);
  _free(alloc2);
  printf("Освобождение второго, финальная проверка\n");
  debug_heap(stdout, HEAP_START);
  
  if(munmap(HEAP_START, HEAP_PAGE_SIZE)==0){
    printf("Тест 2 окончен. Программных ошибок не зафиксировано\n");
  } else {
    fprintf(stderr, "%s", "Error: Ошибка munmap\n");
  }


  printf("Выделение трёх блоков и освобождение двух\n");
  heap_init(HEAP_INITIAL_VALUE);

  void* alloc3 = _malloc(BLOCK_SIZE);
  if(alloc3 == NULL) fprintf(stderr, "%s", "Error: malloc block3 failed\n");
  void* alloc4 = _malloc(BLOCK_SIZE_2);
  if(alloc4 == NULL) fprintf(stderr, "%s", "Error: malloc block4 failed\n");
  void* alloc5 = _malloc(BLOCK_SIZE);
  if(alloc5 == NULL) fprintf(stderr, "%s", "Error: malloc block5 failed\n");
  printf("Аллоцирование трёх блоков\n");
  debug_heap(stdout, HEAP_START);
  _free(alloc3);
  _free(alloc5);
  printf("Освобождение первого и третьего блоков\n");
  debug_heap(stdout, HEAP_START);
  _free(alloc4);
  printf("Освобождение второго блока, финальная проверка\n");
  debug_heap(stdout, HEAP_START);

  if(munmap(HEAP_START, HEAP_PAGE_SIZE)==0){
    printf("Тест 3 окончен. Программных ошибок не зафиксировано\n");
  } else {
    fprintf(stderr, "%s", "Error: Ошибка munmap\n");
  }


  printf("Пробуем заполнить память и расширить регион \n");
  heap_init(HEAP_INITIAL_VALUE);

  debug_heap(stdout, HEAP_START);

  void* alloc6 = _malloc(HEAP_PAGE_SIZE);
  if(alloc6 == NULL) fprintf(stderr, "%s", "Error: malloc block6 failed\n");
  printf("Аллоцирование крупного блока\n");
  debug_heap(stdout, HEAP_START);
  _free(alloc6);
  printf("Освобождение блока, финальная проверка\n");
  debug_heap(stdout, HEAP_START);
  if(munmap(HEAP_START, HEAP_PAGE_SIZE_2)==0){
    printf("Тест 4 окончен. Программных ошибок не зафиксировано\n");
  } else {
    fprintf(stderr, "%s", "Error: Ошибка munmap\n");
  }

  printf("Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте \n");
  heap_init(HEAP_INITIAL_VALUE);

  (void) mmap(HEAP_START + HEAP_PAGE_SIZE, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

  debug_heap(stdout, HEAP_START);

  void* alloc7 = _malloc(BIG_BLOCK_SIZE);
  if(alloc7 == NULL) fprintf(stderr, "%s", "Error: malloc block1 failed\n");

  debug_heap(stdout, HEAP_START);

  _free(alloc7);
  printf("Освобождение блока, финальная проверка\n");
  debug_heap(stdout, HEAP_START);


  return 0;
}
